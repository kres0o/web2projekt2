function isGoodPassword(password) {
    const minLength = 8; 
    const containsUpperCase = /[A-Z]/.test(password); 
    const containsLowerCase = /[a-z]/.test(password); 
    const containsNumber = /[0-9]/.test(password); 
    const containsSpecialChar = /[!@#$%^&*]/.test(password); 
  
    return (
      password.length >= minLength &&
      containsUpperCase &&
      containsLowerCase &&
      containsNumber &&
      containsSpecialChar
    );
  }