const {Client} = require('pg')

export const client = new Client({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    port: 5432,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
})
client.connect()

export const createTables = () => {
    client.query('CREATE TABLE if not exists users (userID int PRIMARY KEY, userName varchar(255), userPass varchar(255), userMail varchar(255), userAdres varchar(255), spol varchar(255), godine int)')
    client.query('CREATE TABLE if not exists autenRI (userID  SERIAL, userMail varchar(255), userPass text)')
    client.query('CREATE TABLE if not exists autenRU (userID  SERIAL, userMail varchar(255), userPass text, salt text)')
} 

export const insertTables = async () => {
    var data = await client.query("SELECT * FROM users")
    if (data.rows.length == 0) {
        client.query("INSERT INTO users VALUES (1, 'kreso', '1234', 'kreso@gmail.com', 'ulica', 'M', 22)")
        client.query("INSERT INTO users VALUES (2, 'kruno', '1234a', 'kreuno@gmail.com', 'ulica 1', 'M', 25)")
        client.query("INSERT INTO users VALUES (3, 'mislav', '12', 'mislav@gmail.com', 'ulica 2', 'M', 25)")
        client.query("INSERT INTO users VALUES (4, 'web2', 'web123', 'web@gmail.com', 'ulica 3', 'Z', 21)")
        client.query("INSERT INTO users VALUES (6, 'karla', '12345', 'karla@gmail.com', 'ulica 13', 'Z', 50);")
    }
}

export const getUsers = async (name, pass) => {
    const data = await client.query("SELECT * FROM users WHERE username = '" + name + "' and userpass = '" + pass + "'");
    return data;
};

export const getUsersRI = async () => {
    const data = await client.query("SELECT * FROM autenRI");
    return data;
};

export const insertUsersRI = async (name, pass) => {
    await client.query(`INSERT INTO "autenri" ("usermail", "userpass") VALUES ($1, $2)`, [name, pass]);
};

export const getUserRI = async (id) => {
    const data = await client.query(`SELECT * FROM autenRI where userid = $1`, [id]);
    return data;
};

export const getUserRILog = async (name, pass) => {
    const data = await client.query(`SELECT * FROM autenRI where usermail = $1 and userpass = $2`, [name, pass]);
    return data;
};

export const getUserRIname = async (name) => {
    const data = await client.query(`SELECT * FROM autenRI where usermail = $1`, [name]);
    return data;
};

export const insertUsersRU = async (name, pass, salt) => {
    await client.query(`INSERT INTO "autenru" ("usermail", "userpass", "salt") VALUES ($1, $2, $3)`, [name, pass, salt]);
};

export const getUserRU = async (id) => {
    const data = await client.query(`SELECT * FROM autenRU where userid = $1`, [id]);
    return data;
};

export const getUsersRU = async () => {
    const data = await client.query("SELECT * FROM autenRU");
    return data;
};

export const getUserRULog = async (name, pass) => {
    const data = await client.query(`SELECT * FROM autenRU where usermail = $1 and userpass = $2`, [name, pass]);
    return data;
};

export const getUserRUname = async (name) => {
    const data = await client.query(`SELECT * FROM autenRU where usermail = $1`, [name]);
    return data;
};