import express from 'express';
import path from 'path';
import session, { SessionData } from 'express-session';
import { Request, Response } from 'express';
import { createTables, getUsers, insertTables, getUsersRI, insertUsersRI, getUserRI, getUserRILog, insertUsersRU, getUsersRU, getUserRUname, getUserRU, getUserRULog, getUserRIname} from './database';
import crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import cookieParser from 'cookie-parser';
import https from 'https';
import fs from 'fs';
import dotenv from 'dotenv';
dotenv.config()

createTables();
insertTables()

function generateSalt(length: number = 16): string {
  return crypto.randomBytes(length).toString('hex');
}

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;
const secretKey = 'your-secret-key';
const jwtOptions = { expiresIn: '1h' };
const axios = require('axios');

function generateToken(user: string | object | Buffer) {
  return jwt.sign(user, secretKey, jwtOptions);
}

const app = express();
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');

app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(session({
  secret: 'mysecret',
  resave: false,
  saveUninitialized: true,
}));

interface MySessionData extends SessionData {
  ranj: boolean;
  user: any;
}

interface CustomRequest extends Request {
  user: any;
}

app.get('/', (req: Request, res: Response) => {
  var user = (req.session as unknown as MySessionData).user
  var ranj = (req.session as unknown as MySessionData).ranj
  if (user == undefined) {
    user = {}
  }
  if (ranj == undefined) {
    ranj = false
  }
  res.render('index', { user, ranj });
});

app.post('/', async (req: Request, res: Response) => {
    //' OR 1=1;--
    var user
    var ranj
    if (req.body.checkboxSQL == undefined) {
      user = (await getUsers(req.body.textSQL, req.body.textSQLpas)).rows;
      ranj = false
    } else {
      user = (await getUsers(req.body.textSQL, req.body.textSQLpas)).rows;
      ranj = true
    }
    (req.session as unknown as MySessionData).user = user;
    (req.session as unknown as MySessionData).ranj = ranj;  

    res.redirect('/');
});

app.get('/auten', (req: Request, res: Response) => {
  res.render('auten');
});

app.post('/auten', async (req: Request, res: Response) => {
  if (req.body.mailUsL == undefined) {
    if (req.body.checkboxAut != undefined) {
      var mailE = (await getUserRIname(req.body.mailUs)).rows
      if (mailE.length > 0) {
        res.send('<script>alert("Mail vec postoji"); window.history.back();</script>')
      } else {
        await insertUsersRI(req.body.mailUs, req.body.pasUs)
        var id = (await getUsersRI()).rows.length
        res.redirect(`/auten/${id}`);
      }
    } else {
      var mailE = (await getUserRUname(req.body.mailUs)).rows
      if (mailE.length > 0) {
        res.send('<script>alert("Greška pri sign up-u"); window.history.back();</script>')
      } else {
        const salt = generateSalt();
        var saltPas = req.body.pasUs + "+-+" + salt
        //console.log(saltPas)
        const sha256 = crypto.createHash('sha256');
        sha256.update(saltPas);
        var hashPas = sha256.digest('hex');
        //console.log(hashPas)
        await insertUsersRU(req.body.mailUs, hashPas, salt)
        
        var id = (await getUsersRU()).rows.length
        const user = { username: req.body.mailUs, userPass: hashPas , userid: id}
        const token = generateToken(user);
        res.cookie('authToken', token, { secure: true, httpOnly: true});
        res.cookie('id', id, { secure: true, httpOnly: true});
        res.redirect(`/auten/sigurna/${id}`);
      }
    }
  } else {
    if (req.body.checkboxAutL != undefined) {
      var user = (await getUserRILog(req.body.mailUsL, req.body.pasUsL)).rows
      if (user.length == 0) {
        res.send('<script>alert("Greška pri log in-u"); window.history.back();</script>')
      } else {
        res.redirect(`/auten/${user[0].userid}`);
      }
    } else {
      const recaptchaResponse = req.body['g-recaptcha-response'];
      const secretKey = '6LeswgcpAAAAAHhroshblfHwmblUDOTPx56vUYht';

      const verificationURL = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${recaptchaResponse}`;

      try {
        const response = await axios.post(verificationURL);
        if (response.data.success) {
          var userName = (await getUserRUname(req.body.mailUsL)).rows
          var salt
          if (userName.length > 0) {
            salt = userName[0].salt
          } else {
            salt = 1
          }
          var saltPas = req.body.pasUsL + "+-+" + salt
          const sha256 = crypto.createHash('sha256');
          sha256.update(saltPas);
          var hashPas = sha256.digest('hex');
          var userV = (await getUserRULog(req.body.mailUsL, hashPas)).rows
          if (userV.length == 0) {
            res.send('<script>alert("Greška pri log in-u"); window.history.back();</script>')
          } else {
            const user = { username: userV[0].usermail, userPass: userV[0].userpass , userid: userV[0].userid}
            const token = generateToken(user);
            res.cookie('authToken', token, { secure: true, httpOnly: true});
            res.cookie('id', userV[0].userid, { secure: true, httpOnly: true});
            res.redirect(`/auten/sigurna/${userV[0].userid}`);
          }
        } else {
          res.send('<script>alert("Greška pri log in-u"); window.history.back();</script>')
        }
      } catch (error) {
        res.send('<script>alert("Greška pri log in-u"); window.history.back();</script>')
      }
    }
  }
});

app.get('/auten/:id', async (req: Request, res: Response) => {
  const id = req.params.id  
  var user = (await getUserRI(id)).rows
  res.render('prijavljen', {user});
});

app.get('/auten/sigurna/:id', async (req: Request, res: Response) => {
  let id = req.params.id  
  var token
  if (id == req.cookies.id) {
    token = req.cookies.authToken as string;
  }

  if (!token) {
    return res.redirect('/auten');
  }

  try {
    const decoded = jwt.verify(token, secretKey);
    (req as CustomRequest).user = decoded;

    const id = req.params.id;
    const user = (req as CustomRequest).user;

    res.render('prijavljenS', { user });
  } catch (ex) {
    res.status(400).json({ error: 'Invalid token' });
  }
});

if (externalUrl) {
  const hostname = '0.0.0.0'; //ne 127.0.0.1
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from
  outside on ${externalUrl}`);
  });
} else {
  https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
  console.log(`Server running at https://localhost:${port}/`);
  });
}
